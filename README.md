# Fortune

Fortune is a python django demo project (in beta). Within the project are 3 apps, only one of which does much of anything
 - The main app is 'card', which currently "reads your fortune" with a 3 card tarot spread. It selects 3 cards randomly from the database, representing Where you stand now / What you aspire to / How to get there. Images of these cards are displayed to the screen. Hovering over each card provides basic information regarding the cards meaning.
 - Next there is the 'landing' app, to store code and templates that should be shared across all project apps (such as the landing page)
 - Finally there is a 'contact' app, which presents a simple form to the user for them to fill in, in order to send a message to the configured gmail address (see Installation instructions below for configuring local environmental variables correctly in order to enable sending)  


### Todos - maybe - eventually
  - Implement semantic HTML tags
  - Overlays in use on card spread page - overlays on mobile are poor UI - find/build alternative
  - Add more tests - should have more
  - Automate the tests so unable to commit unless all tests are passing
  - Clean up card json & migrations file, specifically for "meanings". The current json file is one found online from Daria Chemkaeva. Daria saved me a lot of time as I didn't have to build a json file from scratch with all the card meanings. Unfortunately the format of this file is not quite suitable for a migrations import, especially around the "meanings" variables. If/when I get more time I would alter the json file and remove the need for a hacked work around for the "meanings_evaluated" property on the card model.
  - Consider pushing to a cloud hosting site such as heroku

### Colour Palette
The following colour palette was chosen, based on the favicon.ico file

![colour palette](./readme/colour_palette.png)


### Props
Thanks to the following designers and developers for providing code, images, repos, tutorials etc, upon which this project is based:

*  [Yeshi Kangrang](https://unsplash.com/@omgitsyeshi?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText) - Photo on main landing page!
*  [Daria Chemkaeva](https://www.kaggle.com/lsind18/tarot-json/data) - repo of tarot card info, including images and json file, which the card app migration file is based on.
*  [Freepick on flaticon](https://www.flaticon.com/authors/freepik) for the site favicon


### Tech
The following were used to build this project:

*  python 3
*  django 3
*  bootstrap 4
*  pip3
*  pipenv
*  autopep8 (requires pycodestyle)
*  flake8
*  HTML formatter: https://webformatter.com/html

### Installation (*nix from the command line)
Before you start make sure you have python 3, django 3 and pipenv installed.

Clone this repo to your local machine
```bash
git clone https://bitbucket.org/SinOB/fortune.git
```
navigate into the repo
```bash
cd fortune
```
enable your virtual environment
```bash
pipenv shell
```
Pull in the card data into the db
```bash
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata card.json
```

Configure your gmail settings - so you can send email from the 'contact' app
```bash
export MYEMAIL_HOST_USER='myapp'
export MYEMAIL_HOST_PASSWORD='testing123'
```

run your server
```bash
python manage.py runserver
```

### Tests
To run tests type the following to your command line
```bash
./manage.py test
```

### License info
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Disclaimer: not suitable for production environments

