from django.urls import path, re_path

from .views import contact, success

urlpatterns = [
    re_path(r'^contact/', contact, name='contact'),
    path('success/', success, name='success'),
]
