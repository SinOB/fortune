from django.contrib import admin
from django.urls import path, re_path, include
from landing import views
from cards import views as card_views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^three_card_spread', card_views.three_card_spread,
            name='three_card_spread'),
    path('', include('contact.urls')),
    path('admin/', admin.site.urls),
]
