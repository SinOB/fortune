from django.test import TestCase
from cards.models import Card


class CardModelTest(TestCase):
    def setUp(self):
        Card.objects.create(
            name="my card name",
            number='99',
            meanings='some meaning')

    def test_card_string_representation(self):
        """Card __str__ method returns only the Card name"""
        card = Card(name="My card name")
        self.assertEqual(str(card), card.name)
