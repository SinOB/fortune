from django.db import models
import ast


class Card(models.Model):
    name = models.CharField(max_length=60)
    number = models.IntegerField(null=True)
    img = models.CharField(max_length=60)
    arcana = models.CharField(max_length=60, blank=True, null=True)
    suit = models.CharField(max_length=60, blank=True, null=True)
    fortune_telling = models.CharField(max_length=160, blank=True, null=True)
    keywords = models.CharField(max_length=160, blank=True, null=True)
    meanings = models.CharField(max_length=160)
    archetype = models.CharField(max_length=100, blank=True, null=True)
    mythical_spiritual = models.CharField(
        max_length=100, blank=True, null=True)
    hebrew_alphabet = models.CharField(max_length=60, blank=True, null=True)
    numerology = models.CharField(max_length=100, blank=True, null=True)
    elemental = models.CharField(max_length=100, blank=True, null=True)
    questions_to_ask = models.CharField(max_length=100, blank=True, null=True)
    astrology = models.CharField(max_length=100, blank=True, null=True)
    affirmation = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name

    '''TODO This is a hack. Clean up card json & migrations file,
    specifically for "fortune_telling" and the "meanings".
    The current json file is one found
    online from Daria Chemkaeva. Daria saved me a
    lot of time as I didn't have to build a json file
    from scratch with all the card
    meanings. Unfortunately the format of this file is not
    quite suitable for a migrations import,
    especially around the "meanings" and fortune_telling variables.
    If/when I get more time I would alter the json
    file and remove the need for a hacked work around for the
    "meanings_evaluated" property on the card model.'''
    @property
    def fortune_telling_evaluated(self):
        return ast.literal_eval(self.fortune_telling)
