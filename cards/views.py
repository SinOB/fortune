from django.shortcuts import render
from .models import Card
import random


def three_card_spread(request):

    # get all the cards
    all_cards = Card.objects.all()

    # randomly select 3 cards to display using sample
    # to prevent the same card being chosen multiple times
    choices = random.sample(list(all_cards.values_list('pk', flat=True)), 3)

    # add the 3 choices to context for return to the page
    context = {
        'choice1': Card.objects.get(pk=choices[0]),
        'choice2': Card.objects.get(pk=choices[1]),
        'choice3': Card.objects.get(pk=choices[2]),
    }
    return render(request, 'three_card_spread.html', context)
